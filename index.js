(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.BDF = {})));
}(this, (function (BDF) { 'use strict';
    const decoder = {
        decode(marker) {
            return attemptDecode(marker);
        },
        check: checkDecodeRange,
        consume: consumeDecodeMarker,
        lookahead: lookaheadDecodeMarker,
        next: nextDecodeMarker,
    };
    const encoder = {
        encode(value) {
            encodeMarked(value);
        },
        encodeString(value) {
            if (isCharacter(value)) {
                encoderBuffer.push(114, value.charCodeAt(0));
            } else {
                encoderBuffer.push(115);
                encodeString(value);
            }
        },
        encodeNumber(value) {
            encodeNumber(value, true, getNumberMarker(value));
        },
        encodeNumberAs(value, marker) {
            encodeNumber(value, true, marker);
        }
    };
    const serializersList = new Array();
    const serializersDict = new Dict();
    function register(serializer) {
        const id = serializer.id;
        if (serializersDict.has(id))
            throw `Serializer for ${id} is already in use.`;
        serializersList.push(serializer);
        serializersDict.set(id, serializer);
    }
    BDF.register = register;
    function encode(value) {
        if (value === null)
            return Uint8Array.of(101);
        const type = typeof value;
        switch (type) {
            case "undefined": return Uint8Array.of(100);
            case "boolean": return Uint8Array.of(value ? 103 : 102);
            case "number":
                encodeNumber(value);
                break;
            default:
                encodeMarked(value);
                break;
        }
        return createBuffer();
    }
    BDF.encode = encode;
    function decode(buffer) {
        if (ArrayBuffer.isView(buffer)) {
            decoderBuffer = buffer.buffer;
            decoderU8 = buffer;
            decoderI8 = new Int8Array(buffer.buffer);
            decoderView = new DataView(buffer.buffer);
        }
        else {
            decoderBuffer = buffer;
            decoderU8 = new Uint8Array(buffer);
            decoderI8 = new Int8Array(buffer);
            decoderView = new DataView(buffer);
        }
        decoderOffset = 0;
        decoderLength = buffer.byteLength;
        return attemptDecode(nextDecodeMarker());
    }
    BDF.decode = decode;
    const encodeText = ((encoder) => (value) => {
        return encoder.encode(value);
    })(new TextEncoder());
    const decodeText = ((decoder) => (length) => {
        return decoder.decode(decoderBuffer.slice(decoderOffset, decoderOffset + length));
    })(new TextDecoder());
    const collectionCycleGuard = new WeakSet();
    let collectionDataMarker = -1, collectionLength = -1;
    const encoderBuffer = [];
    let decoderOffset = 0, decoderBuffer, decoderU8, decoderI8, decoderView, decoderLength = 0;
    function attemptDecode(marker) {
        switch (marker) {
            case 100: return undefined;
            case 101: return null;
            case 103: return true;
            case 102: return false;
            case 104: return decodeI8();
            case 105: return decodeU8();
            case 106: return decodeI16();
            case 107: return decodeU16();
            case 108: return decodeI32();
            case 109: return decodeU32();
            case 112: return decodeF32();
            case 113: return decodeF64();
            case 114: return String.fromCharCode(decodeI8());
            case 115: return decodeString();
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 131:
            case 132:
            case 130:
            case 129:
            case 133:
                return decodeArrayBufferView(marker);
            case 116: return new List(decodeList());
            case 118: return decodeList();
            case 117: return new Dict(decodeDict());
            case 119: return decodeDict();
            case 134: {
                const id = decodeString();
                if (serializersDict.has(id))
                    return serializersDict.get(id).decode(decoder);
                throw `There is no custom serializer for ${id}.`;
            }
        }
        throw new Error(`Marker ${Marker[marker] || marker} is unexpected or not yet supported.`);
    }
    function createArrayBufferView(TYPE, length) {
        checkDecodeRange(length);
        return new TYPE(decoderBuffer.slice(decoderOffset, decoderOffset + length));
    }
    function decodeArrayBufferView(marker) {
        const length = decodeLength();
        switch (marker) {
            case 123: return createArrayBufferView(Int8Array, length);
            case 124: return createArrayBufferView(Uint8Array, length);
            case 125: return createArrayBufferView(Int16Array, length * 2);
            case 126: return createArrayBufferView(Uint16Array, length * 2);
            case 127: return createArrayBufferView(Int32Array, length * 4);
            case 128: return createArrayBufferView(Uint32Array, length * 4);
            case 131: return createArrayBufferView(Float32Array, length * 4);
            case 132: return createArrayBufferView(Float64Array, length * 8);
            case 133: return createArrayBufferView(Uint8ClampedArray, length);
        }
        throw `ArrayBuffer view marker ${Marker[marker]} is not yet supported.`;
    }
    function decodeDict() {
        decodeCollectionFlags();
        const object = Object.create(null);
        if (length !== -1) {
            const decode = collectionDataMarker === -1
                ? attemptDecode
                : attemptDecode.bind(null, collectionDataMarker);
            for (let i = 0; i < length; i++) {
                const attr = decodeString();
                object[attr] = decode();
            }
        }
        else {
            while (!decodeCollectionEnd()) {
                const id = decodeString();
                object[id] = attemptDecode(nextDecodeMarker());
            }
            consumeDecodeMarker();
        }
        return object;
    }
    function decodeCollectionEnd() {
        const marker = lookaheadDecodeMarker();
        if (decoderOffset >= decoderLength)
            throw "Unexpected EOF.";
        return marker === 120;
    }
    function decodeList() {
        decodeCollectionFlags();
        const length = collectionLength;
        if (collectionDataMarker !== -1) {
            switch (collectionDataMarker) {
                case 101: return new Array(length).fill(null);
                case 103: return new Array(length).fill(true);
                case 102: return new Array(length).fill(false);
                case 114:
                    checkDecodeRange(length);
                    return Array.from({ length }, () => {
                        return String.fromCharCode(decoderI8[decoderOffset++]);
                    });
                case 115:
                    return Array.from({ length }, () => {
                        return decodeString();
                    });
                default: return Array.from({ length }, attemptDecode.bind(null, collectionDataMarker));
            }
        }
        if (length !== -1) {
            const list = new Array(length);
            for (let i = 0; i < length; i++) {
                list[i] = attemptDecode(nextDecodeMarker());
            }
            return list;
        }
        const list = [];
        while ( !decodeCollectionEnd() ) {
            list.push(attemptDecode(nextDecodeMarker()));
        }
        consumeDecodeMarker();
        return list;
    }
    function decodeCollectionFlags() {
        collectionDataMarker = -1;
        collectionLength = -1;
        switch (lookaheadDecodeMarker()) {
            case 121:
                consumeDecodeMarker();
                collectionDataMarker = nextDecodeMarker();
                collectionLength = decodeLength();
                return;
            case 122:
                consumeDecodeMarker();
                collectionLength = decodeLength();
                return;
        }
    }
    function decodeString() {
        const length = decodeLength();
        checkDecodeRange(length);
        const string = decodeText(length);
        decoderOffset += length;
        return string;
    }
    function decodeF64() {
        checkDecodeRange(8);
        const value = decoderView.getFloat64(decoderOffset);
        decoderOffset += 8;
        return value;
    }
    function decodeF32() {
        checkDecodeRange(4);
        const value = decoderView.getFloat32(decoderOffset);
        decoderOffset += 4;
        return value;
    }
    function decodeLength() {
        const length = attemptDecode(nextDecodeMarker());
        if (length < 0 || !Number.isInteger(length))
            throw 'Invalid length/count.';
        return length;
    }
    function decodeI32() {
        checkDecodeRange(4);
        const value = decoderView.getInt32(decoderOffset);
        decoderOffset += 4;
        return value;
    }
    function decodeU32() {
        checkDecodeRange(4);
        const value = decoderView.getUint32(decoderOffset);
        decoderOffset += 4;
        return value;
    }
    function decodeI16() {
        checkDecodeRange(2);
        const value = decoderView.getInt16(decoderOffset);
        decoderOffset += 2;
        return value;
    }
    function decodeU16() {
        checkDecodeRange(2);
        const value = decoderView.getUint16(decoderOffset);
        decoderOffset += 2;
        return value;
    }
    function decodeU8() {
        checkDecodeRange(1);
        return decoderU8[decoderOffset++];
    }
    function decodeI8() {
        checkDecodeRange(1);
        return decoderI8[decoderOffset++];
    }
    function checkDecodeRange(length) {
        if (decoderOffset + length > decoderLength) {
            throw "Unexpected EOF.";
        }
    }
    function consumeDecodeMarker() {
        checkDecodeRange(1);
        decoderOffset++;
    }
    function lookaheadDecodeMarker() {
        const marker = nextDecodeMarker();
        decoderOffset--;
        return marker;
    }
    function nextDecodeMarker() {
        let marker = 100;
        while (marker === 100 && decoderOffset < decoderLength) {
            marker = decoderU8[decoderOffset++];
        }
        return marker;
    }
    function encodeMarked(value, path = "value") {
        const marker = getMarker(value);
        if (marker === 119) {
            for (const serializer of serializersList) {
                if (serializer.check(value)) {
                    encoderBuffer.push(134);
                    encodeString(serializer.id);
                    serializer.encode(value, encoder);
                    return;
                }
            }
        }
        encoderBuffer.push(marker);
        encodeUnmarked(value, marker, path);
    }
    function encodeUnmarked(value, marker, path) {
        switch (marker) {
            case 101:
            case 102:
            case 103:
            case 100:
                return;
            case 114:
                encoderBuffer.push(value.charCodeAt(0));
                return;
            case 115:
                return encodeString(value);
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 131:
            case 132:
            case 130:
            case 129:
            case 133:
                return encodeArrayBufferView(marker, value);
            case 105:
            case 104:
            case 106:
            case 107:
            case 108:
            case 109:
            case 112:
            case 113:
                return encodeNumber(value, false, marker);
            case 116:
                return encodeList(value.toArray());
            case 118:
                return encodeList(value, path);
            case 117:
                return encodeDict(value.toObject());
            case 119:
                return encodeDict(value, path);
        }
        throw `TODO: marker encoding ${Marker[marker]}.`;
    }
    function encodeDict(value, path = "value") {
        if (collectionCycleGuard.has(value))
            throw `Unexpected cyclic member ${path}.`;
        collectionCycleGuard.add(value);
        let attrs = Object.getOwnPropertyNames(value);
        if (attrs.length <= 2) {
            attrs.forEach(attr => {
                encodeString(attr);
                encodeMarked(value[attr], dict_sub_path(attr, path));
            });
            encoderBuffer.push(Marker.COLLECTION_END);
        }
        else {
            const marker = attrs.map(attr => {
                return getMarker(value[attr]);
            }).reduce((a, b) => {
                return a === b ? a : undefined;
            });
            if (marker === undefined) {
                encoderBuffer.push(Marker.COLLECTION_IS_FIXED);
                encodeNumber(attrs.length);
                attrs.forEach(attr => {
                    encodeString(attr);
                    encodeMarked(value[attr], dict_sub_path(attr, path));
                });
            }
            else {
                encoderBuffer.push(Marker.COLLECTION_IS_TYPED);
                encoderBuffer.push(marker);
                encodeNumber(attrs.length);
                attrs.forEach(attr => {
                    encodeString(attr);
                    encodeUnmarked(value[attr], marker, dict_sub_path(attr, path));
                });
            }
        }
        collectionCycleGuard.delete(value);
    }
    function dict_sub_path(attr, path) {
        return attr.length > 8 ? String.raw `${path}["${attr}"]` : `${path}.${attr}`;
    }
    function encodeList(value, path = "value") {
        if (collectionCycleGuard.has(value))
            throw `Unexpected cyclic member ${path}.`;
        collectionCycleGuard.add(value);
        if (value.length <= 4) {
            value.forEach((item, index) => {
                encodeMarked(item, `${path}[${index}]`);
            });
            encoderBuffer.push(120);
        }
        else {
            let marker = value.map(getMarker).reduce((a, b) => {
                return a === b ? a : undefined;
            });
            if (marker === undefined) {
                encoderBuffer.push(122);
                encodeNumber(value.length);
                value.forEach((item, index) => {
                    encodeMarked(item, `${path}[${index}]`);
                });
            }
            else {
                encoderBuffer.push(121);
                encoderBuffer.push(marker);
                encodeNumber(value.length);
                value.forEach((item, index) => {
                    encodeUnmarked(item, marker, `${path}[${index}]`);
                });
            }
        }
        collectionCycleGuard.delete(value);
    }
    function encodeArrayBufferView(marker, value) {
        switch (marker) {
            case 133:
            case 124:
            case 123:
                {
                    encodeNumber(value.length);
                    encoderBuffer.push.apply(encoderBuffer, value);
                }
                return;
            case 126:
            case 125:
                {
                    const length = value.length;
                    encodeNumber(length);
                    for (let i = 0; i < length; i++) {
                        const short = value[i];
                        encoderBuffer.push((short & 0xFF00) >> 8, (short & 0x00FF));
                    }
                }
                return;
            case 128:
            case 127:
                {
                    const length = value.length;
                    encodeNumber(length);
                    for (let i = 0; i < length; i++) {
                        const int = value[i];
                        encoderBuffer.push((int & 0xFF000000) >> 24, (int & 0x00FF0000) >> 16, (int & 0x0000FF00) >> 8, (int & 0x000000FF));
                    }
                }
                return;
            case 132:
            case 131:
                {
                    encodeNumber(value.length);
                    encoderBuffer.push.apply(encoderBuffer, new Uint8Array(value.buffer));
                }
                return;
        }
        throw `Unsupported ArrayBuffer view of type ${value.constructor.name}`;
    }
    function encodeNumber(value, marked = true, marker) {
        if (marked) {
            marker = getNumberMarker(value);
            encoderBuffer.push(marker);
        }
        switch (marker) {
            case 105:
            case 104:
                {
                    encoderBuffer.push(value);
                }
                return;
            case 106:
            case 107:
                {
                    const short = value;
                    encoderBuffer.push((short & 0xFF00) >> 8, (short & 0x00FF));
                }
                return;
            case 108:
            case 109:
                {
                    const int = value;
                    encoderBuffer.push((int & 0xFF000000) >> 24, (int & 0x00FF0000) >> 16, (int & 0x0000FF00) >> 8, (int & 0x000000FF));
                }
                return;
            case 112:
                {
                    BDF.union.float = value;
                    encoderBuffer.push.apply(encoderBuffer, BDF.union.b4);
                }
                return;
            case 113:
                {
                    BDF.union.double = value;
                    encoderBuffer.push.apply(encoderBuffer, BDF.union.b8);
                }
                return;
        }
    }
    function encodeString(value) {
        encodeNumber(value.length);
        encoderBuffer.push.apply(encoderBuffer, encodeText(value));
    }
    function getBufferViewMarker(name) {
        switch (name) {
            case "Int8Array": return 123;
            case "Uint8Array": return 124;
            case "Int16Array": return 125;
            case "Uint16Array": return 126;
            case "Int32Array": return 127;
            case "Uint32Array": return 128;
            case "Float32Array": return 131;
            case "Float64Array": return 132;
            case "BigInt64Array": return 129;
            case "BigUint64Array": return 130;
            case "Uint8ClampedArray": return 133;
        }
        throw `Unsupported ArrayBuffer view name "${name}".`;
    }
    function getNumberMarker(value) {
        if (Number.isInteger(value)) {
            if (value >= 0) {
                if (value <= 255)
                    return 105;
                if (value <= 65535)
                    return 107;
                if (value <= 4294967295)
                    return 109;
            }
            else {
                if (value >= -128)
                    return 104;
                if (value >= -32768)
                    return 106;
                if (value >= -2147483648)
                    return 108;
            }
        }
        return Math.fround(value) === value ? 112 : 113;
    }
    function getMarker(value) {
        const type = typeof value;
        switch (type) {
            case "undefined": return 100;
            case "boolean": return value ? 103 : 102;
            case "number": return getNumberMarker(value);
            case "string": return isCharacter(value) ? 114 : 115;
            case "object": {
                if (value === null) {
                    return 101;
                }
                else {
                    if (ArrayBuffer.isView(value))
                        return getBufferViewMarker(value.constructor.name);
                    if (List.isList(value))
                        return 116;
                    if (Array.isArray(value))
                        return 118;
                    return Dict.isDict(value) ? 117 : 119;
                }
            }
        }
        throw `Unsupported JS type ${type}.`;
    }
    function isCharacter(value) {
        return value.length === 1 && value.charCodeAt(0) <= 127;
    }
    function createBuffer() {
        const buffer = new Uint8Array(encoderBuffer);
        encoderBuffer.splice(0);
        return buffer;
    }
    let Marker;
    (function (Marker) {
        Marker[Marker["SKIP"] = 100] = "SKIP";
        Marker[Marker["NULL"] = 101] = "NULL";
        Marker[Marker["FALSE"] = 102] = "FALSE";
        Marker[Marker["TRUE"] = 103] = "TRUE";
        Marker[Marker["I8"] = 104] = "I8";
        Marker[Marker["U8"] = 105] = "U8";
        Marker[Marker["I16"] = 106] = "I16";
        Marker[Marker["U16"] = 107] = "U16";
        Marker[Marker["I32"] = 108] = "I32";
        Marker[Marker["U32"] = 109] = "U32";
        Marker[Marker["I64"] = 110] = "I64";
        Marker[Marker["U64"] = 111] = "U64";
        Marker[Marker["F32"] = 112] = "F32";
        Marker[Marker["F64"] = 113] = "F64";
        Marker[Marker["CHAR"] = 114] = "CHAR";
        Marker[Marker["STRING"] = 115] = "STRING";
        Marker[Marker["LIST"] = 116] = "LIST";
        Marker[Marker["DICT"] = 117] = "DICT";
        Marker[Marker["ARRAY"] = 118] = "ARRAY";
        Marker[Marker["OBJECT"] = 119] = "OBJECT";
        Marker[Marker["COLLECTION_END"] = 120] = "COLLECTION_END";
        Marker[Marker["COLLECTION_IS_TYPED"] = 121] = "COLLECTION_IS_TYPED";
        Marker[Marker["COLLECTION_IS_FIXED"] = 122] = "COLLECTION_IS_FIXED";
        Marker[Marker["I8A"] = 123] = "I8A";
        Marker[Marker["U8A"] = 124] = "U8A";
        Marker[Marker["I16A"] = 125] = "I16A";
        Marker[Marker["U16A"] = 126] = "U16A";
        Marker[Marker["I32A"] = 127] = "I32A";
        Marker[Marker["U32A"] = 128] = "U32A";
        Marker[Marker["I64A"] = 129] = "I64A";
        Marker[Marker["U64A"] = 130] = "U64A";
        Marker[Marker["F32A"] = 131] = "F32A";
        Marker[Marker["F64A"] = 132] = "F64A";
        Marker[Marker["U8CA"] = 133] = "U8CA";
        Marker[Marker["CUSTOM"] = 134] = "CUSTOM";
    })(Marker = BDF.Marker || (BDF.Marker = {}));
    BDF.union = (function () {
        const buffer = new ArrayBuffer(8);
        const b2 = new Uint8Array(buffer, 0, 2);
        const b4 = new Uint8Array(buffer, 0, 4);
        const b8 = new Uint8Array(buffer);
        const dtview = new DataView(buffer);
        return {
            b2, b4, b8,
            get short() { return dtview.getInt16(0); },
            set short(value) {
                dtview.setInt16(0, value);
            },
            get ushort() { return dtview.getUint16(0); },
            set ushort(value) {
                dtview.setUint16(0, value);
            },
            get int() { return dtview.getInt32(0); },
            set int(value) {
                dtview.setInt32(0, value);
            },
            get uint() { return dtview.getUint32(0); },
            set uint(value) {
                dtview.setUint32(0, value);
            },
            get float() { return dtview.getFloat32(0); },
            set float(value) {
                dtview.setFloat32(0, value);
            },
            get double() { return dtview.getFloat64(0); },
            set double(value) {
                dtview.setFloat64(0, value);
            },
        };
    })();
})));