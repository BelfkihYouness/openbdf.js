declare namespace BDF {
    export interface Encoder {
        encode<T>(value: T): void;
        encodeString(value: string): void;
        encodeNumber(value: number): void;
        encodeNumberAs(value: number, marker?: Marker): void;
    }
    export interface Decoder {
        consume(): void;
        lookahead(): Marker;
        next(): Marker;
        check(length: number): void;
        decode<T>(marker: Marker): T;
    }
    export interface Serializer<T = any> {
        readonly id: string;
        check(value: any): value is T;
        decode(decoder: Decoder): T;
        encode(value: T, encoder: Encoder): void;
    }
    export function register(serializer: Serializer): void;
    export function encode<T>(value: T): Uint8Array;
    export function decode<T>(buffer: Uint8Array | ArrayBuffer): T;
    export const enum Marker {
        SKIP = 100,
        NULL = 101,
        FALSE = 102,
        TRUE = 103,
        I8 = 104,
        U8 = 105,
        I16 = 106,
        U16 = 107,
        I32 = 108,
        U32 = 109,
        I64 = 110,
        U64 = 111,
        F32 = 112,
        F64 = 113,
        CHAR = 114,
        STRING = 115,
        LIST = 116,
        DICT = 117,
        ARRAY = 118,
        OBJECT = 119,
        COLLECTION_END = 120,
        COLLECTION_IS_TYPED = 121,
        COLLECTION_IS_FIXED = 122,
        I8A = 123,
        U8A = 124,
        I16A = 125,
        U16A = 126,
        I32A = 127,
        U32A = 128,
        I64A = 129,
        U64A = 130,
        F32A = 131,
        F64A = 132,
        U8CA = 133,
        CUSTOM = 134,
    }
    export const union: {
        b2: Uint8Array;
        b4: Uint8Array;
        b8: Uint8Array;
        short: number;
        ushort: number;
        int: number;
        uint: number;
        float: number;
        double: number;
    };
}

declare module "bdf" {
    export = BDF;
}